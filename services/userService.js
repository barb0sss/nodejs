const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
	
	create(data) {
		const item = UserRepository.create(data);
        return item;
	}
	
	update(id, data) {
		if (this.getUserById(id)) {
			const item = UserRepository.update(id, data);
			return item;
		} else {
			return;
		}
	}
	
	delete(id) {
        return UserRepository.delete(id);
	}
	
	getAll() {
		return UserRepository.getAll();
	}
	
	getUserById(userId) {
		const list = UserRepository.dbContext.value();
		return list.find(it => it.id === userId);
	}
}

module.exports = new UserService();