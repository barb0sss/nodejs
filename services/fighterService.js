const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
	
	getAll() {
		return FighterRepository.getAll();
	}
	
	create(data) {
		const item = FighterRepository.create(data);
        return item;
	}
	
	update(id, data) {
		if (this.getFighterById(id)) {
			const item = FighterRepository.update(id, data);
			return item;
		} else {
			return;
		}
	}
	
	delete(id) {
        return FighterRepository.delete(id);
	}
	
	getFighterById(userId) {
		const list = FighterRepository.dbContext.value();
		return list.find(it => it.id === userId);
	}
}

module.exports = new FighterService();