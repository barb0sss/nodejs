const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// TODO: Implement route controllers for user

// Get List of all users
router.get('/', function(req, res, next) {
	res.send(UserService.getAll());
});

// Get User by ID
router.get('/:id', function (req, res) {
	const userData = UserService.getUserById( req.params.id );
	if (userData !== undefined) {
		res.status(200).send( userData );
	} else {
		res.status(404).json({ error: true, message: `User with id:${req.params.id} not found` });
	}
})

// Update User by ID
router.put('/:id', updateUserValid, function (req, res) {
	const userData = UserService.update( req.params.id, req.body );
	if (userData !== undefined) {
		res.status(200).send( userData );
	} else {
		res.status(404).json({ error: true, message: `User with id:${req.params.id} not found` });
	}
})

// Delete User by ID
router.delete('/:id', function (req, res) {
	const result = UserService.delete( req.params.id );
	if (result.length) {
		res.status(200).send(result);
	} else {
		res.status(404).json({ error: true, message: `User with id:${req.params.id} not found` });
	}
})

// POST user (register new user)
router.post('/', createUserValid, (req, res, next) => {
    try {
		
		const data = UserService.create(req.body);
		
        res.data = data;
		
	
    } catch (err) {
        res.err = err;
		console.log(err);
    } finally {
        next();
    }
}, responseMiddleware);



module.exports = router;