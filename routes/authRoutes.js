const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/login', function(req, res, next) {
  res.send(`Login data!`);
});

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action
		const data = req.body;
		
        res.data = data;
		
		if (!data.email || !data.password) {
			res.status(400).json({ error: true,   message: 'Please enter your email and password!' });
			return;
		}
		let result = AuthService.login(data);
		
		if (result) {
			console.log('logged in');
			console.log(result);
			res.status(200).json({message: result});
			next();
		}
    } catch (err) {
        res.err = err;
		console.log(err);
		res.status(404).json({ error: true,   message: err.message });
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;