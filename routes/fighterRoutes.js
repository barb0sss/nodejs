const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

// Get List of all fighters
router.get('/', function(req, res, next) {
	res.send(FighterService.getAll());
});


// Get Fighter by ID
router.get('/:id', function (req, res) {
	const fighterData = FighterService.getFighterById( req.params.id );
	if (fighterData !== undefined) {
		res.status(200).send( fighterData );
	} else {
		res.status(404).json({ error: true, message: `Fighter with id:${req.params.id} not found` });
	}
})


// Update Fighter by ID
router.put('/:id', updateFighterValid, function (req, res) {
	const fighterData = FighterService.update( req.params.id, req.body );
	if (fighterData !== undefined) {
		res.status(200).send( fighterData );
	} else {
		res.status(404).json({ error: true, message: `Fighter with id:${req.params.id} not found` });
	}
})

// Delete Fighter by ID
router.delete('/:id', function (req, res) {
	const result = FighterService.delete( req.params.id );
	if (result.length) {
		res.status(200).send(result);
	} else {
		res.status(404).json({ error: true, message: `Fighter with id:${req.params.id} not found` });
	}
})


// POST fighter (register new fighter)
router.post('/', createFighterValid, (req, res, next) => {
    try {		
		const data = FighterService.create(req.body);
		
        res.data = data;
	
    } catch (err) {
        res.err = err;
		console.log(err);
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;