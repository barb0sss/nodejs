const { fighter } = require('../models/fighter');

function isFull(str) {
    return (str && 0 !== str.length);
}

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation	
	
	const id = req.body.id;
	const power = req.body.power;
	const name = req.body.name;
	const health = req.body.health;
	const defense = req.body.defense;
	
	const regexp_letters = /^[A-Za-z]+$/g;
	let isValidName = ( isFull(name) && ( name.match(regexp_letters) !== null ) );
	let isValidPower = ( isFull(power) && Number.isInteger(parseInt(power)) && power < 100 && power > 0);
	let isValidHealth = ( isFull(health) && Number.isInteger(parseInt(health)) && health <= 100 && health > 0);
	let isValidDefense = ( isFull(defense) && Number.isInteger(parseInt(defense)) && defense <= 10 && defense >= 1);
	
	if ( isValidName && 
	isValidPower && 
	isValidHealth &&
	isValidDefense && 
	!isFull(id) ) {
		req.body.power = power;
		req.body.health = health;
		req.body.defense = defense;
		next();
	} else {		
		if (!isValidName) {
			res.status(400).json({ error: true,   message: 'Please enter fighters name (only letters)' });
			return;
		}
		if (!isValidPower) {
			res.status(400).json({ error: true,   message: 'Fighter\'s power must be less then 100' });
			return;
		}
		if (!isValidHealth) {
			res.status(400).json({ error: true,   message: 'Fighter\'s health must be less then 100' });
			return;
		}
		if (!isValidDefense) {
			res.status(400).json({ error: true,   message: 'Fighter\'s defense must be less then 10' });
			return;
		}
		if (isFull(id)) {
			res.status(400).json({ error: true,   message: 'There\'s ID parameter in your request. Not good!' });
			return;
		}
	}
	
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
	const id = req.body.id;
	const power = req.body.power;
	const name = req.body.name;
	const health = req.body.health;
	const defense = req.body.defense;
	
	const regexp_letters = /^[A-Za-z]+$/g;
	let isValidName = ( isFull(name) && ( name.match(regexp_letters) !== null ) );
	
	
	if (typeof name !== 'undefined') {
		if (!isValidName) {
			res.status(400).json({ error: true,   message: 'Please enter fighters name (only letters)' });
			return;
		}
	}
	
	if (typeof power !== 'undefined') {
		if (!Number.isInteger(parseInt(power)) || power >= 100) {
			res.status(400).json({ error: true,   message: 'Fighter\'s power must be less then 100' });
			return;
		}
	}
	
	if (typeof health !== 'undefined') {
		if (!Number.isInteger(parseInt(health)) || health > 100) {
			res.status(400).json({ error: true,   message: 'Fighter\'s health must be less then 100' });
			return;
		}
	}
	if (typeof defense !== 'undefined') {
		if (!Number.isInteger(parseInt(defense)) || defense > 10) {
			res.status(400).json({ error: true,   message: 'Fighter\'s defense must be less then 10' });
			return;
		}
	}
	if (typeof id !== 'undefined' || isFull(id)) {
		res.status(400).json({ error: true,   message: 'There\'s ID parameter in your request. Not good!' });
		return;
	}
	
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;