const responseMiddleware = (req, res, next) => {
	// TODO: Implement middleware that returns result of the query
	if (res.data) {
		res.status(200).send(res.data);
		next();
	} else {
		res.status(400).json({ error: true,   message: 'Some error..' });
	}
}

exports.responseMiddleware = responseMiddleware;