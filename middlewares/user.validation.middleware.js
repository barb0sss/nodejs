const { user } = require('../models/user');

function isFull(str) {
    return (str && 0 !== str.length);
}

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
	
	
	const id = req.body.id;
	const email = req.body.email;
	const firstName = req.body.firstName;
	const lastName = req.body.lastName;
	const phoneNumber = req.body.phoneNumber;
	const password = req.body.password;
	
	const regexp_letters = /^[A-Za-z]+$/g;
	const regexp_gmail = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@(?:gmail|GMAIL)([\.])(?:com|COM)/g;
	const regexp_phone = /\+380([0-9]{9})/g;
	
	let isValidFirstName = ( isFull(firstName) && ( firstName.match(regexp_letters) !== null ) );
	let isValidLastName = ( isFull(lastName) && ( lastName.match(regexp_letters) !== null ) );
	let isValidEmail = ( isFull(email) && ( email.match(regexp_gmail) !== null ) );
	let isValidPhone = ( isFull(phoneNumber) && ( phoneNumber.match(regexp_phone) !== null ) );
	let isValidPassword = ( isFull(password) && ( password.length >= 6 ) );
	
	if ( isValidEmail && isValidFirstName && isValidLastName && isValidPhone && isValidPassword && !isFull(id) ) {		
		next();
	} else {		
		if (!isValidFirstName) {
			res.status(400).json({ error: true,   message: 'Please enter your first name (only letters)' });
			return;
		}
		if (!isValidLastName) {
			res.status(400).json({ error: true,   message: 'Please enter your last name (only letters)' });
			return;
		}
		if (!isValidEmail) {
			res.status(400).json({ error: true,   message: 'Email is invalid or not belongs to GMAIL' });
			return;
		}
		if (!isValidPhone) {
			res.status(400).json({ error: true,   message: 'Phone number is invalid (must match +380xxxxxxxxx)' });
			return;
		}
		if (!isValidPassword) {
			res.status(400).json({ error: true,   message: 'Password must be at least 6 characters long' });
			return;
		}
		if (isFull(id)) {
			res.status(400).json({ error: true,   message: 'There\'s ID parameter in your request. Not good!' });
			return;
		}
		
	}
	
	
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
	
	const id = req.body.id;
	const email = req.body.email;
	const firstName = req.body.firstName;
	const lastName = req.body.lastName;
	const phoneNumber = req.body.phoneNumber;
	const password = req.body.password;
	
	const regexp_letters = /^[A-Za-z]+$/g;
	const regexp_gmail = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@(?:gmail|GMAIL)([\.])(?:com|COM)/g;
	const regexp_phone = /\+380([0-9]{9})/g;
	
	let isValidFirstName = ( isFull(firstName) && ( firstName.match(regexp_letters) !== null ) );
	let isValidLastName = ( isFull(lastName) && ( lastName.match(regexp_letters) !== null ) );
	let isValidEmail = ( isFull(email) && ( email.match(regexp_gmail) !== null ) );
	let isValidPhone = ( isFull(phoneNumber) && ( phoneNumber.match(regexp_phone) !== null ) );
	let isValidPassword = ( isFull(password) && ( password.length >= 6 ) );
			
		if (typeof firstName !== 'undefined') {
			if (!isValidFirstName) {
				res.status(400).json({ error: true,   message: 'Please enter your first name (only letters)' });
				return;
			}
		}
		if (typeof lastName !== 'undefined') {
			if (!isValidLastName) {
				res.status(400).json({ error: true,   message: 'Please enter your last name (only letters)' });
				return;
			}
		}
		if (typeof email !== 'undefined') {
			if (!isValidEmail) {
				res.status(400).json({ error: true,   message: 'Email is invalid or not belongs to GMAIL' });
				return;
			}
		}
		if (typeof phoneNumber !== 'undefined') {
			if (!isValidPhone) {
				res.status(400).json({ error: true,   message: 'Phone number is invalid (must match +380xxxxxxxxx)' });
				return;
			}
		}
		if (typeof password !== 'undefined') {
			if (!isValidPassword) {
				res.status(400).json({ error: true,   message: 'Password must be at least 6 characters long' });
				return;
			}
		}
		if (typeof id !== 'undefined' || isFull(id)) {
			res.status(400).json({ error: true,   message: 'There\'s ID parameter in your request. Not good!' });
			return;
		}
		
	next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;